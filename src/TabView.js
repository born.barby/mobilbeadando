import React, {Component} from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { TabView, SceneMap } from 'react-native-tab-view';
import MainScreen from "./MainScreen";
import DieteticCenterScreen from "./DieteticCenterMapScreen";
import { WebView } from 'react-native-webview';


const MainRoute = () => (
  // <View style={[styles.scene, { backgroundColor: '#ff4081' }]} />
 <MainScreen/>
);

const HelpRoute = () => (
 // <View style={[styles.scene, { backgroundColor: '#ff4081' }]} />
  <DieteticCenterScreen/>
);
const BasicPrinciples = () => (
  <WebView source={{ uri: 'https://www.hazipatika.com/taplalkozas/egeszseg_es_gasztronomia/cikkek/az_egeszseges_taplalkozas_12_pontja/20020827162201' }} />
);
export default class TabViewExample extends Component {
  state = {
    index: 0,
    routes: [
      { key: 'main', title: 'Főoldal' },
      { key: 'help', title: 'Segítséget kérek' },
      { key: 'principles', title: 'Alapelvek'}
    ],
  };

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({
          main: MainRoute,
          help: HelpRoute,
          principles: BasicPrinciples
        })}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{ width: Dimensions.get('window').width }}
      />
    );
  }
}

