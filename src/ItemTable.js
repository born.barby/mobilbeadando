import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Table, Row, Rows } from 'react-native-table-component';
 
export default class ItemTable extends Component {

    state = {
      tableHead: ['Energia', 'Fehérje', 'Zsír', 'Szénhidrát'],
      tableData: [
        [this.props.kcal + " kcal", this.props.protein + " g", this.props.fat + " g", this.props.ch + " g"],
      ],
      title: this.props.title
    };
  
 
  render() {
    const state = this.state;
    return (
      <View style={styles.container}>
      <Text style={styles.text}>
            {this.state.title}
          </Text>
        <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
          <Row data={state.tableHead} style={styles.head} textStyle={styles.text}/>
          <Rows data={state.tableData} textStyle={styles.text}/>
        </Table>
      </View>
    )
  }
}
 
const styles = StyleSheet.create({
  container: { flex: 1, paddingTop: 30, backgroundColor: '#FAFAD2' },
  head: { height: 40, backgroundColor: '#f1f8ff'},
  text: { margin: 6, textAlign: 'center' }
});