import React from "react";
import { StyleSheet, FlatList } from "react-native";


import ListItem from "./ListItem";

const dataList = props => {
  return (
    <FlatList
      style={styles.listContainer}
      data={props.listElements}
      renderItem={(info) => (
        <ListItem
          data={info.item.value}
          image={info.item.image}
          onItemPressed={() => props.onItemSelected(info.item.key)}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  listContainer: {
    width: "100%",
    backgroundColor: '#FFEBCD'
  }
});

export default dataList;
