import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import meatImage from "./meat.jpg";
import animalOrigImage from "./animal_orig.jpg";
import plantOrigImage from "./plant_orig.jpg";
import basicFoodImage from "./food.jpg";
import chImage from "./ch.jpg";
import dietImage from "./diet.jpg";


const listItem = props => (
  <TouchableOpacity onPress={props.onItemPressed}>
    <View style={styles.listItem}>
      <Image resizeMode="cover" source={getImageFromSource(props.image)} style={styles.listImage}/>
      <Text>{props.data}</Text>
    </View>
  </TouchableOpacity>
);
const getImageFromSource = src => { 
  switch (src){
    case "./ch.jpg":
      return chImage;
    case "./food.jpg":
      return basicFoodImage;
    case "./diet.jpg":
      return dietImage;
    case "./animal_orig.jpg":
      return animalOrigImage;
    case "./meat.jpg":
      return meatImage;
    case "./plant_orig.jpg":
      return plantOrigImage;
    default:
      return null;
  }
};
const styles = StyleSheet.create({
  listItem: {
    width: "100%",
    marginBottom: 5,
    padding: 10,
    backgroundColor: "#E9967A",
    flexDirection: "row",
    alignItems: "center"
  },
  listImage: {
    marginRight: 10,
    height: 28,
    width: 28
  }
});

export default listItem;