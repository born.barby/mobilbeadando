import React from 'react';
import ItemTable from "./ItemTable";
import { StyleSheet, Modal, View, ScrollView,Image, Text, Button, SafeAreaView } from 'react-native';

const DietElementModal = props => {
    let content = null;

    if (props.selectedItem) {
        content = (
          <ScrollView>
            <View>
            <Text style={styles.text}>Név: {props.selectedItem.value}</Text>  
                <Text style={styles.text}>Mennyiség: {props.selectedItem.amount} {props.selectedItem.measurement}</Text>  
                <ItemTable
                  kcal = {props.selectedItem.kcal} ch = {props.selectedItem.ch} fat = {props.selectedItem.fat} protein = {props.selectedItem.protein} title="A fenti mennyiségre vonatkoztatott tápérték adatok: "/>
            </View>
            </ScrollView>
        );
    }

    return (
        <Modal 
            visible={props.selectedItem !== null} 
            onRequestClose={props.onModalDismissed} 
            animationType="slide"
            style = {styles.modal}>
            <ScrollView style={styles.container}>
                <View style={styles.container}>
                    {content}
                    <View>
                        <Button title="Ok" onPress={props.onModalDismissed}/>
                        <Button title="Törlés" onPress={props.onItemDeleted} disabled={props.disabled}/>
                    </View>
                </View>
            </ScrollView>
        </Modal>
    );
}

const styles = StyleSheet.create({
    modal: {
        margin: 20,
        backgroundColor: '#FFEBCD'
    },
    text: {
        fontSize: 14,
        fontWeight: "bold",
        textAlign: "center"
    },
    container: {
    backgroundColor: '#FFEBCD',
    padding: 8,
  },
});

export default DietElementModal;