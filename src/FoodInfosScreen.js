import  React, {Component} from 'react';
import { StyleSheet, Button, View, ScrollView, Text, Alert, } from 'react-native';
// You can import from local files
import ListModal from "./ListModal";
import DataList from "./DataList";
import DataInput from "./DataInput";
import AddNewModal from "./AddNewModal";
import DietPlannerScreen from "./DietPlannerScreen";
import meatImage from "./meat.jpg";
import animalOrigImage from "./animal_orig.jpg";
import plantOrigImage from "./plant_orig.jpg";
import basicFoodImage from "./food.jpg";
import chImage from "./ch.jpg";
import ItemArray from '../data/ItemArray.json';


export default class FoodInfosScreen extends Component {
  
  macroFilter = "";
  valueFilter = "";


    state = {
    title: "Étel információk",
    selectedItem: null,
    editableItem: null,
    filteredItemArray : ItemArray.array,
    appearAddNewItem : false
  };
     addNewItemHandler = () => {
    this.setState(prevState => {
      return {
        appearAddNewItem: true
      };
    });
  };
    addNewModelDismissedHandler = () =>{
this.setState({
       appearAddNewItem : false,
       editableItem : null
    });
    this.props.navigation.navigate("Étel információk", {actArray : ItemArray.array})
  };
    itemSelectedHandler = key => {
    this.setState(prevState => {
      return {
        selectedItem: prevState.filteredItemArray.find( item => {
          return item.key === key;
        })
      };
    });
  };

    itemDeletedHandler = key => {
      this.setState({ visible: true });
      ItemArray.array = ItemArray.array.filter(item => {
          return item.key !== this.state.selectedItem.key && item.value !== this.state.selectedItem.value;
        });
    this.setState(prevState => {
      return {
        filteredItemArray: prevState.filteredItemArray.filter(item => {
          return item.key !== prevState.selectedItem.key && item.value !== prevState.selectedItem.value;
        }),
        selectedItem: null,
      };
    });
  };
  editItemHandler = key =>{
    this.setState(prevState => { return{
      appearAddNewItem : true,
      editableItem : prevState.selectedItem,
      selectedItem: null
    };});

  
  };
        itemSaveHandler = (value, macros, kcal, protein, fat, ch, measurement, image )=> {
    this.setState(prevState => {
      var key = Math.random();
      if(prevState.editableItem !== null){
        ItemArray.array = ItemArray.array.filter(item => item.value !== prevState.editableItem.value && item.key !== this.state.editableItem.key)
        key = prevState.editableItem.key;
      }
      ItemArray.array.push({key: key,
          value: value,
          macros: macros,
          kcal: kcal,
          protein: protein,
          fat: fat,
          ch: ch,
          measurement: measurement,
          image: image});
      
      
      this.createOkButtonAlert("Siker!", "Az étel sikeresen hozzá lett adva/szerkesztve lett.");
      
      return {
        filteredItemArray: ItemArray.array
      };
   
    });
    this.inputMacroSearchHandler(this.macroFilter);
    this.inputSearchHandler(this.valueFilter);
    this.addNewModelDismissedHandler();
  };
      inputMacroSearchHandler = userValue => {

    this.setState(prevState => {
      this.macroFilter = userValue.trim();
      if(this.valueFilter === "" && this.macroFilter === ""){
        return {filteredItemArray: ItemArray.array};
      }else if(this.macroFilter=== "" && this.valueFilter !== ""){
        return {
        filteredItemArray: ItemArray.array.filter(item => {
          return item.value.includes(this.valueFilter);

        })
        };
      }else if(this.macroFilter !== "" && this.valueFilter !== ""){
      return {
        filteredItemArray: prevState.filteredItemArray.filter(item => {
          return item.macros.includes(this.macroFilter);

        })
      };
      }else{
        return {
        filteredItemArray: ItemArray.array.filter(item => {
          return item.macros.includes(this.macroFilter);

        })
      };
      }
      
    });
      };
        modelDismissedHandler = () => {
    this.setState({
       selectedItem: null
    });
  }

    inputSearchHandler = userValue => {

    this.setState(prevState => {
      this.valueFilter = userValue.trim();
      if(this.valueFilter === "" && this.macroFilter === ""){
        return {filteredItemArray: ItemArray.array};
      }else if(this.valueFilter=== "" && this.macroFilter !== ""){
               return {
        filteredItemArray: ItemArray.array.filter(item => {
          return item.macros.includes(this.macroFilter);

        })
      };
      }else if(this.valueFilter !== "" && this.macroFilter !== ""){
      return {
        filteredItemArray: prevState.filteredItemArray.filter(item => {
          return item.value.includes(this.valueFilter);

        })
      };
      }else{
        return {
        filteredItemArray: ItemArray.array.filter(item => {
          return item.value.includes(this.valueFilter);

        })
      };
      }
      
    });
  };

       createConfirmationAlert = key =>
    Alert.alert(
      "Megerősítés",
      "Biztos vagy benne?",
      [
        
        {
          text: "Cancel",
          onPress: () => {},
          style: "cancel"
        },
        { text: "OK", onPress: () => this.itemDeletedHandler(key) }
      ],
      { cancelable: false }
    );
     createOkButtonAlert = (title, message) =>
    Alert.alert(
      title,
      message,
      [
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ],
      { cancelable: false }
    );
  render() {
    return(
    <ScrollView style={styles.container}>
    <View style={styles.container}>
    <ListModal 
          selectedItem={this.state.selectedItem} 
          selectButtonTitle="Törlés"
          onItemSelected={this.createConfirmationAlert} 
          onModalDismissed={this.modelDismissedHandler}
          disabled = {false}
          onItemEdited = {this.editItemHandler}
          title={"Tápérték adatok (100 g/ml-re vonatkoztatva)"}/>
          <AddNewModal 
           appear = {this.state.appearAddNewItem}
          onItemSaved={this.itemSaveHandler} 
          onModalDismissed={this.addNewModelDismissedHandler}
          itemToEdit = {this.state.editableItem}
          />
    <Text style={styles.title}>
            {this.state.title}
          </Text>
<DataInput onInputButton={this.inputSearchHandler} buttonTitle="Keresd" inputHint="Keresés név alapján..." visible={true}/>
<DataInput onInputButton={this.inputMacroSearchHandler} buttonTitle="Keresd" inputHint="Keresés makro alapján..." visible={true}/>
<Button title="Új étel" color="#D2691E" onPress={this.addNewItemHandler}/>
               <DataList
               visible={true}
          listElements={this.state.filteredItemArray}
          onItemSelected={this.itemSelectedHandler}
        />
<Button title="Étrend tervezés" color="#D2691E" onPress={() => {DietPlannerScreen.shouldChange = true;this.props.navigation.navigate("Étrend tervező", { actDiet: null })}} />
    </View>
    </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#FFEBCD',
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    paddingBottom: 10,
  }
});