import React, { Component } from 'react';
import ItemTable from "./ItemTable";
import { TextInput, StyleSheet, Modal, View, ScrollView,Image, Text, Button, SafeAreaView, Alert } from 'react-native';
import meatImage from "./meat.jpg";
import animalOrigImage from "./animal_orig.jpg";
import plantOrigImage from "./plant_orig.jpg";
import basicFoodImage from "./food.jpg";
import chImage from "./ch.jpg";
export default class AddNewModal extends Component {
    content = null;
    state = {
    value: "",
    macros: "",
    kcal: "",
    protein: "",
    fat: "",
    ch: "",
    measurement: ""

  };
   
  
    
   valueChangedHandler = val => {
    this.setState({
      value: val.trim()
    });
  };
     macrosChangedHandler = val => {
    this.setState({
      macros: val.trim()
    });
  };
   kcalChangedHandler = val => {
    this.setState({
      kcal: val.trim()
    });
  };
  proteinChangedHandler = val => {
    this.setState({
      protein: val.trim()
    });
  };
    fatChangedHandler = val => {
    this.setState({
      fat: val.trim()
    });
  };
       chChangedHandler = val => {
    this.setState({
      ch: val.trim()
    });
  };
     measurementChangedHandler = val => {
    this.setState({
      measurement: val.trim()
    });
  };
     createOkButtonAlert = (title, message) =>
    Alert.alert(
      title,
      message,
      [
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ],
      { cancelable: false }
    );
    checkMacroValue= macro =>{
      var splitted = String(macro).split(",");
      var prefImage = undefined;
      switch(splitted[0]){
        case "Fehérje":
          prefImage = "./meat.jpg";
          break;
        case "Rost":
          prefImage = "./plant_orig.jpg";
        break;
        case "Vitamin":
          prefImage = "./plant_orig.jpg";
        break;
        case "Széhnhidrát":
          prefImage = "./ch.jpg";
          break;
        case "Zsír":
          prefImage = "./animal_orig.jpg";
          break;
        default:
          break;
      }
      if(prefImage === undefined){
      this.createOkButtonAlert("Hiba!","Nem megfelelő makro érték!");
      }
      return prefImage;
    };
    checkPropertiesOfNewItem= () =>{
   if((this.state.value==="" || this.state.macros === "" || this.state.kcal === "" || this.state.protein === "" || this.state.fat === "" || this.state.ch==="" || this.state.measurement==="")){
      this.createOkButtonAlert("Hiba!","Minden adat kitöltése kötelező!");
    }else if(this.state.measurement.trim().toLowerCase() !== "g" && this.state.measurement.trim().toLowerCase() !== "ml"){
          this.createOkButtonAlert("Hiba!","Hibás mértékegység, csak g vagy ml lehet!");
    }else{
      var kcal = parseFloat(this.state.kcal);
      var protein = parseFloat(this.state.protein);
      var fat = parseFloat(this.state.fat);
      var ch = parseFloat(this.state.ch);
      if(isNaN(kcal)|| isNaN(protein) || isNaN(fat) || isNaN(ch)){
        this.createOkButtonAlert("Hiba!","Hibás Energia/Fehérje/Zsír/Szénhidrát érték!");
      }else{
        var prefImage = this.checkMacroValue(this.state.macros.trim());  
      if(prefImage !== undefined){
  this.resetState();
        this.props.onItemSaved(this.state.value.trim(), this.state.macros.trim(), kcal, protein, fat, ch, this.state.measurement.trim().toLowerCase(), prefImage);
      
      }
    }
    }};
    checkPropertiesOfEditedItem = () =>{
      var correct = true;
      var prefImage;
      var value = this.props.itemToEdit.value;
      var macros = this.props.itemToEdit.macros;
    if(this.state.value.trim() !== ""){
        value = this.state.value.trim();
      }
      if(this.state.macros.trim()!==""){
        macros = this.state.macros.trim();
      }
        prefImage = this.checkMacroValue(macros);
        correct = prefImage !== undefined;
        if(correct){
          var flKcal;
          var flCh;
          var flProtein;
          var flFat;
             if(this.state.kcal.trim() === ""){
        flKcal = parseFloat(this.props.itemToEdit.kcal);
      }else{
        flKcal = parseFloat(this.state.kcal.trim());
      }
if(this.state.ch.trim() === ""){
         flCh = parseFloat(this.props.itemToEdit.ch)
      }else{
        flCh = parseFloat(this.state.ch.trim());
      }
        if(this.state.protein.trim() === ""){
        flProtein =  parseFloat(this.props.itemToEdit.protein);
      }else{
        flProtein = parseFloat(this.state.protein.trim());
      }
             if(this.state.fat.trim() === ""){
        flFat =  parseFloat(this.props.itemToEdit.fat);
      }else{
        flFat = parseFloat(this.state.fat.trim());
        
      }
      correct = !isNaN(flFat) && !isNaN(flKcal) && !isNaN(flProtein) && !isNaN(flCh);
      if(!correct){
this.createOkButtonAlert("Hiba!","Hibás Energia/Fehérje/Zsír/Szénhidrát érték!");
      }
        }
      if(correct){
      if(this.state.measurement.trim() === ""){
        this.setState({measurement : this.props.itemToEdit.measurement});
      }else if(this.state.measurement.trim().toLowerCase() === "g" || this.state.measurement.trim().toLowerCase() === "ml"){
        this.setState((prevState) => {return {measurement : prevState.measurement.trim().toLowerCase()}; });
        
        
      }else{
        correct = false;
this.createOkButtonAlert("Hiba!","Hibás mértékegység, csak g vagy ml lehet!");
      }
        
      }
      if(correct){
        this.resetState();
        this.props.onItemSaved(value, macros, flKcal, flProtein,           flFat, flCh, this.state.measurement, prefImage);
      }
};
resetState = () => {
this.setState({value: "", macros: "", kcal: "", fat: "", protein: "", ch: "", measurement: ""});
};
  checkProperties = () =>{
    if(this.props.itemToEdit === null){
      this.checkPropertiesOfNewItem();
 
    }else{
      this.checkPropertiesOfEditedItem();
  
    }
  };
  render() {
    return (
        <Modal 
            visible={this.props.appear} 
            onRequestClose={this.props.onModalDismissed} 
            animationType="slide">
            <ScrollView style={styles.container}>
                <View style={styles.container}>
      
            <View>
            <Text>Név: {this.props.itemToEdit !== null ? "(Eredeti: " + this.props.itemToEdit.value + ")": ""}</Text>
          <TextInput
          placeholder= "Ide írj..."
          value={this.state.value}
          onChangeText={this.valueChangedHandler}
          style={styles.input}
        />
        <Text>Makró(k): {this.props.itemToEdit !== null ? "(Eredeti: " + this.props.itemToEdit.macros + ")": ""}</Text>
          <TextInput
          placeholder= "Ha több, akkor ,-vel elválasztva..."
          value={this.state.macros}
          onChangeText={this.macrosChangedHandler}
          style={styles.input}
        />
        <Text>Energia(kcal)/100 g/ml: {this.props.itemToEdit !== null ? "(Eredeti: " + this.props.itemToEdit.kcal + " g)": ""}</Text>
          <TextInput
          placeholder= "Ide írj..."
          value={this.state.kcal}
          onChangeText={this.kcalChangedHandler}
          style={styles.input}
        />
        <Text>Fehérje(g)/100 g/ml: {this.props.itemToEdit !== null ? "(Eredeti: " + this.props.itemToEdit.protein + " g)": ""}</Text>
         <TextInput
          placeholder= "Ide írj..."
          value={this.state.protein}
          onChangeText={this.proteinChangedHandler}
          style={styles.input}
        />
    
      <Text>Zsír(g)/100 g/ml: {this.props.itemToEdit !== null ? "(Eredeti: " + this.props.itemToEdit.fat + " g)": ""}</Text>
         <TextInput
          placeholder= "Ide írj..."
          value={this.state.fat}
          onChangeText={this.fatChangedHandler}
          style={styles.input}
        />
        <Text>Szénhidrát(g)/100 g/ml: {this.props.itemToEdit !== null ? "(Eredeti: " + this.props.itemToEdit.ch + " g)": ""}</Text>
            <TextInput
          placeholder= "Ide írj..."
          value={this.state.ch}
          onChangeText={this.chChangedHandler}
          style={styles.input}
        />
         <Text>Mértékegység (g/ml): {this.props.itemToEdit !== null ? "(Eredeti: " + this.props.itemToEdit.measurement + ")": ""}</Text>
                  <TextInput
          placeholder= "Ide írj..."
          value={this.state.measurement}
          onChangeText={this.measurementChangedHandler}
          style={styles.input}
        />
            </View>
            
                    <View>
                        <Button title="Mentés" color="red" onPress={this.checkProperties}/>
                        <Button title="Bezárás" onPress={() => {this.resetState(); this.props.onModalDismissed();}}/>
                    </View>
                </View>
            </ScrollView>
        </Modal>
    );
  }
}


const styles = StyleSheet.create({
    container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#FFEBCD',
  },
    input: {
       height: 40, borderColor: 'black', borderWidth: 1 
    }
});

