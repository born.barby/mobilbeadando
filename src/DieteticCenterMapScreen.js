import React, { Component } from "react";
import { View, StyleSheet, Button } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker }  from 'react-native-maps';

export default class DieteticCenterScreen extends Component {
    static navigationOptions = {
      title: 'Deitetikai központ',
    };
    render() {
    //  const {navigate} = this.props.navigation;
      return (
              <View style={styles.container}>
          <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          region={{
            latitude: 47.480000,
            longitude: 19.069200,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}>
            <Marker 
              title="Dietetikus központ"
              coordinate={{
                latitude: 47.480000, 
                longitude: 19.069200,
              }}
              anchor={{ x: 0.69, y: 1 }} />
            </MapView>
      </View>
   
      );
    }
  }
  const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
   justifyContent: 'flex-end',
   alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject
  }
});