import  React, {Component} from 'react';
import { StyleSheet, Button, View, ScrollView, Text, Alert, } from 'react-native';
// You can import from local files
import ListModal from "./ListModal";
import DataList from "./DataList";
import DataInput from "./DataInput";
import DietListModal from "./DietListModal";
import DietPlannerScreen from "./DietPlannerScreen";
import DietArray from '../data/DietArray.json';

export default class DietsScreen extends Component {
  
  nameFilter = "";
  dateFilter = "";


    state = {
    title: "Étrendeim",
    selectedItem: null,
    filteredDietArray : this.props.route.params.dietArray,
    appearAddNewItem : false
  };
     static getDerivedStateFromProps(props, state) {

    if (props.route.params.dietArray !== state.filteredDietArray) {
      return {
        filteredDietArray: props.route.params.dietArray
      };
    }
    return null;
  }
    itemSelectedHandler = key => {
    this.setState(prevState => {
      return {
        selectedItem: prevState.filteredDietArray.find( item => {
          return item.key === key;
        })
      };
    });
  };

  addNewItemHandler = () => {
    this.setState(prevState => {
      return {
        appearAddNewItem: true
      };
    });
  };
    itemDeletedHandler = key => {

      DietArray.array = DietArray.array.filter(item => {
          return item.name !== this.state.selectedItem.name && item.value !== this.state.selectedItem.value;
        });
    this.setState(prevState => {
      return {
        filteredDietArray: prevState.filteredDietArray.filter(item => {
          return item.name !== this.state.selectedItem.name && item.value !== this.state.selectedItem.value;
        }),
        selectedItem: null,
      };
    });
    this.props.navigation.navigate("Étrendeim", {dietArray : DietArray.array});
  };
      inputDateSearchHandler = userValue => {

    this.setState(prevState => {
      this.dateFilter = userValue.trim();
      if(this.nameFilter === "" && this.dateFilter === ""){
        return {filteredDietArray: DietArray.array};
      }else if(this.dateFilter=== "" && this.nameFilter !== ""){
        return {
        filteredDietArray: DietArray.array.filter(item => {
          return item.name.includes(this.nameFilter);

        })
        };
      }else if(this.dateFilter !== "" && this.nameFilter !== ""){
      return {
        filteredDietArray: prevState.filteredDietArray.filter(item => {
          return item.value.includes(this.dateFilter);

        })
      };
      }else{
        return {
        filteredDietArray: DietArray.array.filter(item => {
          return item.value.includes(this.dateFilter);

        })
      };
      }
      
    });
      };
        modelDismissedHandler = () => {
    this.setState({
       selectedItem: null
    });
  }
  addNewModelDismissedHandler = () =>{
this.setState({
       appearAddNewItem : false
    });
  };
    inputNameSearchHandler = userValue => {

    this.setState(prevState => {
      this.nameFilter = userValue.trim();
      if(this.nameFilter === "" && this.dateFilter === ""){
        return {filteredDietArray: DietArray.array};
      }else if(this.nameFilter=== "" && this.dateFilter !== ""){
               return {
        filteredDietArray: DietArray.array.filter(item => {
          return item.value.includes(this.dateFilter);

        })
      };
      }else if(this.nameFilter !== "" && this.dateFilter !== ""){
      return {
        filteredDietArray: prevState.filteredDietArray.filter(item => {
          return item.name.includes(this.nameFilter);

        })
      };
      }else{
        return {
        filteredDietArray: DietArray.array.filter(item => {
          return item.name.includes(this.nameFilter);

        })
      };
      }
      
    });
  };

       createConfirmationAlert = key =>
    Alert.alert(
      "Megerősítés",
      "Biztos vagy benne?",
      [
        
        {
          text: "Cancel",
          onPress: () => {},
          style: "cancel"
        },
        { text: "OK", onPress: () => this.itemDeletedHandler(key) }
      ],
      { cancelable: false }
    );
     createOkButtonAlert = (title, message) =>
    Alert.alert(
      title,
      message,
      [
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ],
      { cancelable: false }
    );
  
  render() {
    return(
    <ScrollView style={styles.container}>
    <View style={styles.container}>
    <DietListModal 
          selectedItem={this.state.selectedItem} 
          disabled={false}
          selectButtonTitle="Törlés"
          onItemDeleted={this.createConfirmationAlert} 
          onModalDismissed={this.modelDismissedHandler}
          saveModifiedDiet = {null}
          title = "Összesített tápérték adatok"/>
    <Text style={styles.title}>
            {this.state.title}
          </Text>
<DataInput onInputButton={this.inputNameSearchHandler} buttonTitle="Keresd" inputHint="Keresés dátum alapján..." visible={true}/>
<DataInput onInputButton={this.inputDateSearchHandler} buttonTitle="Keresd" inputHint="Keresés név alapján..." visible={true}/>
<DataList
               visible={true}
          listElements={this.state.filteredDietArray}
          onItemSelected={this.itemSelectedHandler}/>
    </View>
    </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#FFEBCD',
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    paddingBottom: 10,
  }
});