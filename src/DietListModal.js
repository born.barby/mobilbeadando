import React, {Component}from 'react';
import ItemTable from "./ItemTable";
import DataList from "./DataList";
import DietElementModal from "./DietElementModal";
import { StyleSheet, Modal, View, ScrollView,Image, Text, Button, SafeAreaView,Alert } from 'react-native';



export default class DietListModal  extends Component{
 content = null;
    
    breItemSelectedHandler = key => {
    this.setState(prevState => {
      return {
        selectedItem: this.props.selectedItem.breakfast.find( item => {
          return item.key === key;
        }),
        dayTime: "breakfast"
      };
    });
  };
      bruItemSelectedHandler = key => {
    this.setState(prevState => {
      return {
        selectedItem: this.props.selectedItem.brunch.find( item => {
          return item.key === key;
        }),
        dayTime: "brunch"
      };
    });
  };
        lunchItemSelectedHandler = key => {
    this.setState(prevState => {
      return {
        selectedItem: this.props.selectedItem.lunch.find( item => {
          return item.key === key;
        }),
        dayTime: "lunch"
      };
    });
  };
          snackItemSelectedHandler = key => {
    this.setState(prevState => {
      return {
        selectedItem: this.props.selectedItem.snack.find( item => {
          return item.key === key;
        }),
        dayTime: "snack"
      };
    });
  };
          dinnerItemSelectedHandler = key => {
    this.setState(prevState => {
      return {
        selectedItem: this.props.selectedItem.dinner.find( item => {
          return item.key === key;
        }),
        dayTime: "dinner"
      };
    });
  };
  state = {
    selectedItem : null,
    dayTime : ""
  };
        modelDismissedHandler = () => {
    this.setState({
       selectedItem: null
    });
  };
      deleteItem = key => {
        var breakfast = this.props.selectedItem.breakfast;
        var brunch = this.props.selectedItem.brunch;
        var lunch = this.props.selectedItem.lunch;
        var snack = this.props.selectedItem.snack;
        var dinner = this.props.selectedItem.dinner;
        switch(this.state.dayTime){
          case "breakfast":
            breakfast = breakfast.filter(item => item.key !== this.state.selectedItem.key && item.value !== this.state.selectedItem.value);
            break;
          case "brunch":
brunch = brunch.filter(item => item.key !== this.state.selectedItem.key && item.value !== this.state.selectedItem.value);
            break;
          case "lunch":
lunch = lunch.filter(item => item.key !== this.state.selectedItem.key && item.value !== this.state.selectedItem.value);
            break;
          case "snack":
snack = snack.filter(item => item.key !== this.state.selectedItem.key && item.value !== this.state.selectedItem.value);
            break;
          case "dinner":
dinner = dinner.filter(item => item.key !== this.state.selectedItem.key && item.value !== this.state.selectedItem.value);
            break;
          default:
            break;
        }
      var newDiet = {
        key : this.props.selectedItem.key,
        value : this.props.selectedItem.value,
        breakfast : breakfast,
        brunch : brunch,
        lunch : lunch,
        snack : snack,
        dinner : dinner,
        image : this.props.selectedItem.image,
        sumKcal : this.props.selectedItem.sumKcal - this.state.selectedItem.kcal,
        sumCh : this.props.selectedItem.sumCh - this.state.selectedItem.ch,
        sumProtein: this.props.selectedItem.sumProtein - this.state.selectedItem.protein,
        sumFat : this.props.selectedItem.sumFat - this.state.selectedItem.fat
      };
        this.setState({
       selectedItem: null
    });
      this.props.saveModifiedDiet(newDiet);
   
  };
  createConfirmationAlert = key =>
    Alert.alert(
      "Megerősítés",
      "Biztos vagy benne?",
      [
        
        {
          text: "Bezár",
          onPress: () => {},
          style: "cancel"
        },
        { text: "OK", onPress: () => this.deleteItem(key) }
      ],
      { cancelable: false }
    );

render() {
        if(this.props.selectedItem !== null){
        this.content = (
          <ScrollView>
            <View>
            <DietElementModal
            selectedItem={this.state.selectedItem}
            onModalDismissed={this.modelDismissedHandler}
            onItemDeleted={this.createConfirmationAlert}
            disabled = {!this.props.disabled}
            />
                <Text style={styles.text}>Név: {this.props.selectedItem.name}</Text>
                <Text style={styles.text}>Dátum: {this.props.selectedItem.value}</Text>
                <Text styles={styles.text}>Összesített adatok:</Text>
                 <ItemTable
                  kcal = {this.props.selectedItem.sumKcal} ch = {this.props.selectedItem.sumCh} fat = {this.props.selectedItem.sumFat} protein = {this.props.selectedItem.sumProtein} title={this.props.title}/>
                <Text style={styles.text}>Reggeli:</Text>
                <DataList
               visible={true}
          listElements={this.props.selectedItem.breakfast}
          onItemSelected={this.breItemSelectedHandler}/>
                <Text style={styles.text}>Tízórai:</Text>
                <DataList
               visible={true}
          listElements={this.props.selectedItem.brunch}
          onItemSelected={this.brUItemSelectedHandler}/>
                <Text style={styles.text}>Ebéd:</Text>
                <DataList
               visible={true}
          listElements={this.props.selectedItem.lunch}
          onItemSelected={this.lunchItemSelectedHandler}/>
                <Text style={styles.text}>Uzsonna:</Text>
                <DataList
               visible={true}
          listElements={this.props.selectedItem.snack}
          onItemSelected={this.snackItemSelectedHandler}/>
                <Text style={styles.text}>Vacsora:</Text>
                <DataList
               visible={true}
          listElements={this.props.selectedItem.dinner}
          onItemSelected={this.dinnerItemSelectedHandler}/>
            </View>
            </ScrollView>
        );
      }
    return (
        <Modal 
            visible={this.props.selectedItem !== null} 
            onRequestClose={this.props.onModalDismissed} 
            animationType="slide" style = {styles.modal}>
            <ScrollView style={styles.container}>
                <View style={styles.container}>
                    {this.content}
                    <View>
                    <Button title={this.props.selectButtonTitle} color="red" onPress={this.props.onItemDeleted} disabled={this.props.disabled}/>
                        <Button title="Bezár" onPress={this.props.onModalDismissed}/>
                    </View>
                </View>
            </ScrollView>
        </Modal>
    );
}
}

const styles = StyleSheet.create({
    modal: {
        margin: 20,
        backgroundColor: '#FFEBCD'
    },
    text: {
        fontSize: 14,
        fontWeight: "bold",
        textAlign: "center"
    },
    container: {
    backgroundColor: '#FFEBCD',
    padding: 8,
  },
});
