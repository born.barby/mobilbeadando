import React from 'react';
import ItemTable from "./ItemTable";
import { StyleSheet, Modal, View, ScrollView,Image, Text, Button, SafeAreaView } from 'react-native';
import meatImage from "./meat.jpg";
import animalOrigImage from "./animal_orig.jpg";
import plantOrigImage from "./plant_orig.jpg";
import basicFoodImage from "./food.jpg";
import chImage from "./ch.jpg";
import dietImage from "./diet.jpg";


const ListModal = props => {
    let content = null;

    if (props.selectedItem) {
        content = (
          <ScrollView style={styles.container}>
            <View style={styles.container}>
                <Image resizeMode="contain" source={getImageFromSource(props.selectedItem.image)} style={styles.image}/>
                <Text style={styles.text}>Név: {props.selectedItem.value}</Text>
                <Text style={styles.text}>Főbb makro komponen(sek): {props.selectedItem.macros}</Text>
                <ItemTable
                  kcal = {props.selectedItem.kcal} ch = {props.selectedItem.ch} fat = {props.selectedItem.fat} protein = {props.selectedItem.protein} title={props.title}
                />    
            </View>
            </ScrollView>
        );
    }

    return (
        <Modal 
            visible={props.selectedItem !== null} 
            onRequestClose={props.onModalDismissed} 
            animationType="slide">
            <SafeAreaView>
                <View style={styles.modal}>
                    {content}
                    <View>
                        <Button title={props.selectButtonTitle} color="red" onPress={props.onItemSelected}/>
                        <Button title="Szerkesztés" disabled={props.disabled} onPress={props.onItemEdited}/>
                        <Button title="Bezár" onPress={props.onModalDismissed}/>
                    </View>
                </View>
            </SafeAreaView>
        </Modal>
    );
}
const getImageFromSource = src => { 

  switch (src){
    case "./ch.jpg":
      return chImage;
    case "./food.jpg":
      return basicFoodImage;
    case "./diet.jpg":
      return dietImage;
    case "./animal_orig.jpg":
      return animalOrigImage;
    case "./meat.jpg":
      return meatImage;
    case "./plant_orig.jpg":
      return plantOrigImage;
    default:
      return null;
  }
};
const styles = StyleSheet.create({
    modal: {
        margin: 20,
        backgroundColor: '#FFEBCD',
    },
    image: {
        width: "100%",
        height: 250
    },
    container: {
    backgroundColor: '#FFEBCD',
    padding: 8,
  },
    text: {
        fontSize: 14,
        fontWeight: "bold",
        textAlign: "center"
    }
});

export default ListModal;