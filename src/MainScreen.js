import React, {Component} from 'react';
import Constants from 'expo-constants';
import {Text, View, ScrollView, StyleSheet, Button, Image, TextInput, Alert} from 'react-native';
import foodPyramid from './food_pyramid.jpg';



class MainScreen extends Component {
state = {
weight : "",
height: "",
bmiEvaluation: ""
};
     weightChangedHandler = val => {
    this.setState({
      weight: val.trim()
    });
  };
       heightChangedHandler = val => {
    this.setState({
      height: val.trim()
    });
  };
       createOkButtonAlert = (title, message) =>
    Alert.alert(
      title,
      message,
      [
        { text: "OK" }
      ],
      { cancelable: false }
    );
  calculateBmi = val => {
    var flHeight = parseFloat(this.state.height);
    var flWeight = parseFloat(this.state.weight);
    if(isNaN(flHeight) || isNaN(flWeight)){
      this.createOkButtonAlert("Hiba!", "A súlyt és a magasságot is ki kell tölteni, a megfelelő értékekkel!");
    }else{
    var heightInmeter = flHeight/100;
    var bmi = flWeight / (heightInmeter ^2);
    var msg = "A BMI-d: " + String(bmi);
    if(bmi < 18.5){
      this.setState({bmiEvaluation : msg + ", az alultápláltak (BMI: <18.5) kategóriájába tartozol, gyarapodnod kéne, hogy elérd az egészséges kategóriát!"});
    }else if(bmi >=18.5 && bmi <25){
      this.setState({bmiEvaluation : msg + ", a normális testsúlyúak (BMI: 18.5-24.9) kategóriájába tartozol, étkezz egészségesen, hogy megtartsd ezt a kategóriát!"});
    }else if(bmi >= 25 && bmi <30){
this.setState({bmiEvaluation : msg + ", az enyhén túlsúlyosak (BMI: 25-30) kategóriájába tartozol, étkezz minél egészségesebben, figyelj oda a megfelelő tápanyagbevitelre, hogy egészségesebbé válj!"});
    }else{
      this.setState({bmiEvaluation : msg + ", az elhízottak (BMI: >30) kategóriájába tartozol, étkezz minél egészségesebben, figyelj oda a megfelelő tápanyagbevitelre, hogy egészségesebbé válj! Jó ha tudod, hogy az elhízás nagyon sok betegséghez vezethet."});
    }
    }
  }
  render () {
    return (
       <ScrollView>
      <View style={styles.container}>
        <Text style={styles.title}>Étrend tervező</Text>
        <Text>Üdvözöllek!</Text>
        <Text style={styles.text}> Tervezd meg étrendedet, az egészségesebb életmódért.</Text>
        <Text style={styles.text}> Magasságod (cm-ben):</Text>
                 <TextInput
          placeholder= "cm"
          value={this.state.height}
          onChangeText={this.heightChangedHandler}
          style={styles.input}
        />
        <Text style={styles.text}> Súlyod (kg-ban):</Text>
         <TextInput
          placeholder= "kg"
          value={this.state.weight}
          onChangeText={this.weightChangedHandler}
          style={styles.input}
        />
        <Button color="#D2691E" onPress={this.calculateBmi} title="Számolj" />
        <Text style={styles.text}> {this.state.bmiEvaluation}</Text>
        <Image resizeMode="contain" source={foodPyramid} style={styles.image}/>
        <Button color="#D2691E" onPress={() => this.props.navigation.navigate("Étel információk") } title="Étel információk" />
      </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
     flex: 1,
    alignItems: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#FFEBCD',
    padding: 8,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold"
  },
  text:{
    alignItems: 'justified'
  },
  image: {
        width: "100%",
        height: 400
    }, 
    input: {
       height: 40, backgroundColor: 'white', borderColor: 'black', borderWidth: 1, width : 100
    }
});

export default MainScreen;