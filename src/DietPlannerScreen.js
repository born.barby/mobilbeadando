import  React, {Component, useState, useEffect} from 'react';
import { TextInput, StyleSheet, Button, View, ScrollView, Text, Alert, } from 'react-native';
import DatePicker from 'react-native-datepicker';

// You can import from local files
import ListModal from "./ListModal";
import DataList from "./DataList";
import DataInput from "./DataInput";
import { RadioButton } from 'react-native-paper';
import AddNewModal from "./AddNewModal";
import DietListModal from "./DietListModal";
import meatImage from "./meat.jpg";
import animalOrigImage from "./animal_orig.jpg";
import plantOrigImage from "./plant_orig.jpg";
import basicFoodImage from "./food.jpg";
import dietImage from "./diet.jpg";
import chImage from "./ch.jpg";
import ItemArray from '../data/ItemArray.json';
import DietArray from '../data/DietArray.json';
import moment from 'moment';

export default class DietPlannerScreen extends Component {
  macroFilter = "";
  valueFilter = "";

        state = {
    title: "Étrend tervező",
    selectedItem: null,
    origDate : this.props.route.params.actDiet !== null? this.props.route.params.actDiet.value : null,
    origName : this.props.route.params.actDiet !== null? this.props.route.params.actDiet.name : "",
    filteredItemArray : ItemArray.array,
    dietName: this.props.route.params.actDiet !== null? this.props.route.params.actDiet.name : "",
    date: this.props.route.params.actDiet !== null? this.props.route.params.actDiet.value : null,
    listVisible : false,
    actMeal : "breakfast",
    actAmount: "",
    actDiet : this.props.route.params.actDiet !== null? this.props.route.params.actDiet : null,
    sumKcal : this.props.route.params.actDiet !== null? this.props.route.params.actDiet.sumKcal : 0.0,
    sumCh : this.props.route.params.actDiet !== null? this.props.route.params.actDiet.sumCh : 0.0,
    sumProtein: this.props.route.params.actDiet !== null? this.props.route.params.actDiet.sumProtein : 0.0,
    sumFat : this.props.route.params.actDiet !== null? this.props.route.params.actDiet.sumFat : 0.0,
    selectedActDiet : null
  };

showItemList = val =>{
    this.setState(prevState => {
      return {
        selectedActDiet : prevState.actDiet
      };
    });

};
 
    dietNameChangedHandler = val => {
    this.setState({
      dietName: val.trim()
    });
  };
      actAmountChangedHandler = val => {
    this.setState({
      actAmount: val.trim()
    });
  };
    itemSelectedHandler = key => {
      if(this.state.listVisible){
        var flAmount = parseFloat(this.state.actAmount);
        if(isNaN(flAmount) || flAmount<=0.0){
          this.createOkButtonAlert("Hiba!", "Adj meg egy mennyiséget előbb (g/ml-be átszámolva)!");
        }else{
    this.setState(prevState => {
      return {
        selectedItem: prevState.filteredItemArray.find( item => {
          return item.key === key;
        })
      };
    });
        }
      }else{
        this.createOkButtonAlert("Hiba!","Először meg kell adnod a szükséges adatokat (név, dátum), és rá kell kattintani a 'Dátum és név mentése' gombra!");
      }
  };
checkedChange = value => {
    this.setState(prevState => {
      return {
        actMeal : value
      };
    });
};

    itemAddedHandler = key => {
    this.setState(prevState => {
      var actDietItem = prevState.actDiet;
      var flAmount = parseFloat(prevState.actAmount);
      var actKcal = prevState.selectedItem.kcal * (flAmount/100);
      var actCh = prevState.selectedItem.ch * (flAmount/100);
      var actFat = prevState.selectedItem.fat * (flAmount/100);
      var actProtein = prevState.selectedItem.protein *(flAmount/100);
      switch(prevState.actMeal){
        case "breakfast":
            actDietItem.breakfast.push({key: Math.random(), value : this.state.selectedItem.value, image: this.state.selectedItem.image, amount: this.state.actAmount, measurement : this.state.selectedItem.measurement, kcal: actKcal, ch : actCh, fat : actFat, protein: actProtein})
          break;
        case "brunch":
          actDietItem.brunch.push({key: Math.random(), value : this.state.selectedItem.value, amount: this.state.actAmount, measurement : this.state.selectedItem.measurement, kcal: actKcal, ch : actCh, fat : actFat, protein: actProtein})
          break;
        case "lunch":
          actDietItem.lunch.push({key: Math.random(), value : this.state.selectedItem.value, amount: this.state.actAmount, measurement : this.state.selectedItem.measurement, kcal: actKcal, ch : actCh, fat : actFat, protein: actProtein})
          break;
        case "snack":
          actDietItem.snack.push({key: Math.random(), value : this.state.selectedItem.value, amount: this.state.actAmount, measurement : this.state.selectedItem.measurement, kcal: actKcal, ch : actCh, fat : actFat, protein: actProtein})
          break;
        case "dinner":
          actDietItem.dinner.push({key: Math.random(), value : this.state.selectedItem.value, amount: this.state.actAmount, measurement : this.state.selectedItem.measurement, kcal: actKcal, ch : actCh, fat : actFat, protein: actProtein})
          break;
        default:
          break;
      }
      actDietItem.sumKcal = prevState.sumKcal + actKcal;
      actDietItem.sumCh = prevState.sumCh + actCh;
      actDietItem.sumFat = prevState.sumFat + actFat;
      actDietItem.sumProtein = prevState.sumProtein + actProtein;
      return {
        actDiet : actDietItem,
        selectedItem: null,
        sumKcal : prevState.sumKcal + actKcal,
        sumCh : prevState.sumCh + actCh,
        sumFat : prevState.sumFat + actFat,
        sumProtein : prevState.sumProtein + actProtein,
        actAmount : ""
      };
    });
  };
  saveNewDietHandler = key => {
    if(this.state.actDiet.breakfast.length == 0 && this.state.actDiet.brunch.length == 0 && this.state.actDiet.lunch.length == 0 && this.state.actDiet.snack == 0 && this.state.actDiet.dinner == 0){
      this.createOkButtonAlert("Hiba!","Adj hozzá min. 1 ételt!");
    }else{
      if(this.state.origDate !==null && this.state.origName !== ""){
        DietArray.array = DietArray.array.filter(item => item.value !== this.state.origDate && item.name !==this.state.name);
      }
      DietArray.array.push(this.state.actDiet);
      this.refreshState();
      this.createOkButtonAlert("Siker!", "Étrend sikeresen hozzáadva!");
      
      this.props.navigation.navigate("Étrendeim", {dietArray : DietArray.array});
    }
  };
  refreshState = () => {
    this.setState({
    actAmount: "",
    dietName: "",
    date : null,
    actDiet : null,
    sumKcal : 0.0,
    sumCh : 0.0,
    sumProtein: 0.0,
    sumFat : 0.0,
    origDate : null,
    origName : "",
    listVisible : false});
  };
  saveNewDietDatasHandler = key => {
    if(this.state.dietName==="" || this.state.date === null){
      this.createOkButtonAlert("Hiba!", "Minden mező kitöltése kötelező!");
this.setState({listVisible: false});
    }else{
      var breakfast = [];
      var brunch = [];
      var lunch = [];
      var snack = [];
      var dinner = [];
      if(this.state.origDate != null && this.state.origName !== ""){
        breakfast = this.state.actDiet.breakfast;
        brunch = this.state.actDiet.brunch;
        lunch = this.state.actDiet.lunch;
        snack = this.state.actDiet.snack;
        dinner = this.state.actDiet.dinner;
      }
    var dietElement = {
      key : Math.random(),
      name : this.state.dietName,
      value : this.state.date,
      breakfast : breakfast,
      brunch : brunch,
      lunch : lunch,
      snack : snack,
      dinner : dinner,
      image : "./diet.jpg",
      sumKcal : this.state.sumKcal,
      sumCh : this.state.sumCh,
      sumProtein : this.state.sumProtein,
      sumFat : this.state.sumFat
    }
    this.setState({actDiet: dietElement});
    this.setState({listVisible: true});
    this.createOkButtonAlert("Siker!", "A dátum és név mentése sikeres volt!");
    }

    
  }
      inputMacroSearchHandler = userValue => {

    this.setState(prevState => {
      this.macroFilter = userValue.trim();
      if(this.valueFilter === "" && this.macroFilter === ""){
        return {filteredItemArray: ItemArray.array};
      }else if(this.macroFilter=== "" && this.valueFilter !== ""){
        return {
        filteredItemArray: ItemArray.array.filter(item => {
          return item.value.includes(this.valueFilter);

        })
        };
      }else if(this.macroFilter !== "" && this.valueFilter !== ""){
      return {
        filteredItemArray: prevState.filteredItemArray.filter(item => {
          return item.macros.includes(this.macroFilter);

        })
      };
      }else{
        return {
        filteredItemArray: ItemArray.array.filter(item => {
          return item.macros.includes(this.macroFilter);

        })
      };
      }
      
    });
      };
        modelDismissedHandler = () => {
    this.setState({
       selectedItem: null
    });
  }
    inputSearchHandler = userValue => {

    this.setState(prevState => {
      this.valueFilter = userValue.trim();
      if(this.valueFilter === "" && this.macroFilter === ""){
        return {filteredItemArray: ItemArray.array};
      }else if(this.valueFilter=== "" && this.macroFilter !== ""){
               return {
        filteredItemArray: ItemArray.array.filter(item => {
          return item.macros.includes(this.macroFilter);

        })
      };
      }else if(this.valueFilter !== "" && this.macroFilter !== ""){
      return {
        filteredItemArray: prevState.filteredItemArray.filter(item => {
          return item.value.includes(this.valueFilter);

        })
      };
      }else{
        return {
        filteredItemArray: ItemArray.array.filter(item => {
          return item.value.includes(this.valueFilter);

        })
      };
      }
      
    });
  };
      itemSaveHandler = (value, macros, kcal, protein, fat, ch, measurement, image )=> {
    this.setState(prevState => {

      ItemArray.array.push({key: Math.random(),
          value: value,
          macros: macros,
          kcal: kcal,
          protein: protein,
          fat: fat,
          ch: ch,
          measurement: measurement,
          image: image});
      
     // this.addNewModelDismissedHandler();
      this.createOkButtonAlert("Siker!", "Az étel sikeresen hozzá lett adva.");
      return {
        filteredItemArray: ItemArray.array
      };
   
    });
  };
       createConfirmationAlert = key =>
    Alert.alert(
      "Megerősítés",
      "Biztos vagy benne?",
      [
        
        {
          text: "Bezár",
          onPress: () => {},
          style: "cancel"
        },
        { text: "OK", onPress: () => this.itemAddedHandler(key) }
      ],
      { cancelable: false }
    );
     createOkButtonAlert = (title, message) =>
    Alert.alert(
      title,
      message,
      [
        { text: "OK", onPress: () => {} }
      ],
      { cancelable: false }
    );
          itemModelDismissedHandler = () => {
    this.setState({
       selectedActDiet: null
    });
  }
saveNewDiet = newDiet =>{
      this.setState(prevState => {
      return {
        actDiet : newDiet, selectedActDiet : null, sumKcal : newDiet.sumKcal, sumFat : newDiet.sumFat, sumCh: newDiet.sumCh, sumProtein: newDiet.sumProtein
      };
    });
};

  render() {

    return(
    <ScrollView>
    <View style={styles.container}>
<DietListModal
            selectedItem={this.state.selectedActDiet}
            onModalDismissed={this.itemModelDismissedHandler}
             disabled = {true}
          selectButtonTitle=""
          onItemDeleted={null}
          saveModifiedDiet={this.saveNewDiet}
          onItemEdited={null}
          title = "Összesített tápérték adatok"
            />
    <ListModal 
          selectedItem={this.state.selectedItem} 
          selectButtonTitle="Hozzáad"
          onItemSelected={this.createConfirmationAlert} 
          onModalDismissed={this.modelDismissedHandler}
          disabled = {true}
          onItemEdited = {null}
          title={"Tápérték adatok (100  g/ml-re vonatkoztatva)"}/>
    <Text style={styles.title}>
            {this.state.title}
            
          </Text>
           <Text>Dátum:</Text>
           <DatePicker
          showIcon={false}
          androidMode="spinner"
          style={{ width: 300 }}
          date={this.state.date}
          mode="date"
          placeholder="YYYY-MM-DD"
          format="YYYY-MM-DD"

          confirmBtnText="Ok"
          cancelBtnText="Bezárás"
          customStyles={{dateInput: {
              backgroundColor: 'white',
              borderWidth: 1,
              borderColor: 'black',
            },}}
          onDateChange={(date) => {
            this.setState({ date: date });
          }}
        />
               <Text>Étrend neve:</Text>
          <TextInput
          placeholder= "Ide írj..."
          value={this.state.dietName}
          onChangeText={this.dietNameChangedHandler}
          style={styles.input}
        />

<Button title="Dátum és név mentése" color="#D2691E" onPress={this.saveNewDietDatasHandler} disabled={this.state.listVisible}/>
<Button title="Eddigi ételek" color="#D2691E" onPress={this.showItemList} disabled={!this.state.listVisible}/>
<Button title="Étrend mentése" color="#D2691E" onPress={this.saveNewDietHandler} disabled={!this.state.listVisible}/>

<View style={{flexDirection: 'row'}}>
<Text>Kcal: {this.state.sumKcal}, Fehérje (g): {this.state.sumProtein}</Text>
</View>
<View style={{flexDirection: 'row'}}>
<Text>Szénhidrát (g): {this.state.sumCh}, Zsír (g): {this.state.sumFat}</Text>
</View>
<View style={{flexDirection: 'row'}}>
<RadioButton
        value="Reggeli"
        status={ this.state.actMeal==="breakfast" ? "checked" : "unchecked"}
        onPress={() => this.checkedChange("breakfast")}
      />
<Text style={{marginLeft: 10}}>Reggeli</Text>
</View>
<View style={{flexDirection: 'row'}}>
 <RadioButton
        value="Tízórai"
        status={ this.state.actMeal==="brunch" ? "checked" : "unchecked"}
        onPress={() => this.checkedChange("brunch")}
      />
      <Text style={{marginLeft: 10}}>Tízórai</Text>
</View>
 <View style={{flexDirection: 'row'}}>
      <RadioButton
        value="Ebéd"
        status={ this.state.actMeal==="lunch" ? "checked" : "unchecked"}
        onPress={() => this.checkedChange("lunch")}
      />
      <Text style={{marginLeft: 10}}>Ebéd</Text>
</View>            
 <View style={{flexDirection: 'row'}}>
                   <RadioButton
        value="Uzsonna"
        status={ this.state.actMeal==="snack" ? "checked" : "unchecked"}
        onPress={() => this.checkedChange("snack")}
      />
      <Text style={{marginLeft: 10}}>Uzsonna</Text>
</View>
<View style={{flexDirection: 'row'}}>
         <RadioButton
        value="Vacsora"
        status={ this.state.actMeal==="dinner" ? "checked" : "unchecked"}
        onPress={() => this.checkedChange("dinner")}
      />
      <Text style={{marginLeft: 10}}>Vacsora</Text>
</View>

<DataInput onInputButton={this.inputSearchHandler} buttonTitle="Keresd" inputHint="Keresés név alapján..." visible={this.state.listVisible}/>
<DataInput onInputButton={this.inputMacroSearchHandler} buttonTitle="Keresd" inputHint="Keresés makro alapján..." visible={this.state.listVisible}/>
<Text>Mennyiség (g/ml):</Text>
         <TextInput
          placeholder= "Ide írj..."
          value={this.state.actAmount}
          onChangeText={this.actAmountChangedHandler}
          style={styles.input}
        />
               <DataList
               visible={this.state.listVisible}
          listElements={this.state.filteredItemArray}
          onItemSelected={this.itemSelectedHandler}
        />
    </View>
    </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: '#FFEBCD',
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    paddingBottom: 10,
  },
          input: {
              backgroundColor: 'white',
              borderWidth: 1,
              borderColor: 'black',
              width: 300
            }
});