import React, { Component } from "react";
import { View, TextInput, Button, StyleSheet } from "react-native";

class DataInput extends Component {
  state = {
    input: ""
  };

  inputChangedHandler = val => {
    this.setState({
      input: val
    });
  };

  inputSubmitHandler = () => {
    this.props.onInputButton(this.state.input);
  };

  render() {
    return (
      <View style={styles.inputContainer}>
        <TextInput
          editable={this.props.visible}
          placeholder={this.props.inputHint}
          value={this.state.input}
          onChangeText={this.inputChangedHandler}
          style={styles.input}
        />
        <Button
          title={this.props.buttonTitle}
          style={styles.button}
          onPress={this.inputSubmitHandler}
          color="#D2691E"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  input: {
    width: "70%"
  },
  button: {
    width: "30%",
  }
});

export default DataInput;