import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Constants from 'expo-constants';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import FoodInfosScreen from "./src/FoodInfosScreen";
import MainScreen from "./src/MainScreen";
import DietPlannerScreen from "./src/DietPlannerScreen";
import DietsScreen from "./src/DietsScreen";
import DietArray from "./data/DietArray.json";
import DieteticCenterScreen from "./src/DieteticCenterMapScreen";
import TabViewExample from "./src/TabView";





// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default function App() {
     const Drawer = createDrawerNavigator();
     DietPlannerScreen.shouldChange = true;

  return (
<NavigationContainer style = {styles.nav}>

       <Drawer.Navigator>
        <Drawer.Screen name="Főoldal" component={TabViewExample} />
        <Drawer.Screen name="Étel információk" component={FoodInfosScreen} />
        <Drawer.Screen name="Étrend tervező" initialParams={{ actDiet: null }} component={DietPlannerScreen} />
        <Drawer.Screen name="Étrendeim" initialParams={{ dietArray: DietArray.array }} component={DietsScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
 nav: {
        backgroundColor: "#E9967A"
    }
});

